package Inventory.business;

import Inventory.data.DBAccess;
import Inventory.presentation.CLI;
import Inventory.sharedClasses.Category;
import Inventory.sharedClasses.Product;
import Inventory.sharedClasses.Sale;
import Inventory.sharedClasses.Stock;

import java.util.ArrayList;

/**
 * Created by Shahar on 29/03/2017.
 */
public class BAL {

    DBAccess dba;
    CLI view;

    public BAL(CLI cli) {
        dba = new DBAccess(this);
        view = cli;
    }

    public void getLowQuantity() {
        ArrayList<Stock> ans = dba.getLowQuantity();
        view.print(ans);
    }

    public void getAllDeffectives() {
        ArrayList<Stock> ans = dba.getAllDefectives();
        for (Stock item : ans) {
            item.setProductName(dba.getProductName(item.getSerialNumber()));
        }
        view.print(ans);
    }

    /**
     * Prints products report
     */
    public void getAllProducts() {
        ArrayList<Product> ans = dba.getAllProducts();
        view.print(ans);
    }

    /**
     * Prints products report by a given list of categories
     *
     * @param categories
     */
    public void getAllProducts(ArrayList<String> categories) {
        ArrayList<Product> ans;
        for (String category : categories) {
            ans = dba.getAllProducts(category);
            view.print(ans);
        }
    }

    /**
     * Alerts the user of below minimal amount of an item
     *
     * @param stock The product with low quantity
     */
    public void alertLowQuanity(Stock stock) {
        String serialNumber = stock.getSerialNumber();
        view.print("Warning! Product " + dba.getProductName(serialNumber) + "(" + serialNumber + ")" +
                " is running out of stock at " + stock.getLocation() + "!\n" +
                "Minimal: " + stock.getMinimal() + ". Remaining: " + stock.getQuantity() + ".");
    }

    public void newCategory(Category cat) {
        if (!dba.insertCategory(cat))
            view.print("Failed inserting new category");
        else
            view.print("Done successfully!");
    }

    public void newProduct(Product p) {
        if (!dba.insertProduct(p))
            view.print("Failed inserting new product");
        else
            view.print("Done successfully!");
    }

    public void newSale(Sale s) {
        if (!dba.insertSale(s))
            view.print("Failed inserting new sale");
        else
            view.print("Done successfully!");
    }

    public void deleteCategory(int catID) {
        if (!dba.removeCategory(catID))
            view.print("Category " + catID + " not found");
        else
            view.print("Done successfully!");
    }

    public void deleteProduct(String serialNum) {
        if (!dba.removeProduct(serialNum))
            view.print("Product " + serialNum + " not found");
        else
            view.print("Done successfully!");
    }

    public void deleteSale(int saleId) {
        if (!dba.removeSale(saleId))
            view.print("Sale number " + saleId + " not found");
        else
            view.print("Done successfully!");
    }

    /**
     * Sets product's category. if category doesn't exists, add it to the database
     *
     * @param choice   1 - data is serial number, 2 - data is products name
     * @param data     the products data
     * @param category the category name
     */
    public void setProductsCategory(int choice, String data, String category) {
        int categoryId = dba.findCategory(category);
        if (categoryId == 0) {
            categoryId = dba.addNewCategory(category);
        }
        if (choice == 1)
            dba.setProductsCategoryBySerialNumber(data, categoryId);
        else
            dba.setProductsCategoryByName(data, categoryId);
    }

    /**
     * Prints report of all the inventory - amounts in all locations
     */
    public void getAllInventory() {
        ArrayList<Stock> ans = dba.getAllInventory();
        view.print(ans);
    }

    /**
     * Prints report of all sales
     */
    public void getAllSales() {
        ArrayList<Sale> ans = dba.getAllSales();
        view.print(ans);
    }

    /**
     * Prints report of all the categories
     */
    public void getAllCategories() {
        ArrayList<Category> ans = dba.getAllCategories();
        view.print(ans);
    }

    public void updateInventory(Stock stock) {
        if (!dba.updateStock(stock))
            view.print("Failed");
        else {
            view.print("Done successfully!");
            String serialNum = stock.getSerialNumber();
            String location = stock.getLocation();
            int difference = getDiff(stock);
            if (difference < 0)
                view.print("Alert! Product number " + serialNum + " in " + location +
                        " is on shortage of " + difference + "!!!");
        }
    }

    public int getDiff(Stock stock) {
        String serialNum = stock.getSerialNumber();
        String location = stock.getLocation();
        int quantity = stock.getQuantity();
        int defectives = stock.getDefectives();
        int minimalQuantity = dba.getProductMinimalQuantity(serialNum, location);
        int difference = quantity - defectives - minimalQuantity;
        return difference;
    }

    public void editSaleField(String field, String value, int saleId) {
        dba.updateSaleField(field, value, saleId);
    }

    public void editCategoryField(String field, String value, int id) {
        dba.updateCategoryField(field, value, id);
    }

    public void editProductField(String field, String value, int serialNumber) {
        dba.updateProductField(field, value, serialNumber);
    }

    /**
     * puts items on an existing sale
     * @param choice 1 - data is product's serial number, 2 - data is category's id
     * @param data the data
     * @param saleId the sale id
     */
    public void addItemsToSale(int choice, String data, int saleId) {
        if (choice == 1)
            if (!dba.addItemToSaleByProduct(data, saleId))
                view.print("Failed");
            else
                view.print("Done successfully!");
        else {
            boolean success = true;
            ArrayList<Product> ans;
            int catId = Integer.parseInt(data);
            ans = dba.getAllProducts(catId);
            for (Product p : ans) {
                if (!dba.addItemToSaleByProduct(p.getSerialNumber(), saleId))
                    success = false;
            }
            if (!success)
                view.print("Failed");
            else
                view.print("Done successfully!");
        }
    }

    /**
     * Prints all items on a given sale
     * @param saleId the sale's id
     */
    public void showAllItemsOnSale(int saleId) {
        ArrayList<Product> ans = dba.getAllItemsOnSale(saleId);
        if (ans == null)
            view.print("Failed");
        else {
            if (ans.isEmpty())
                view.print("No items on this sale!");
            else
                view.print(ans);
        }
    }

    /**
     * Remove product from an existing sale
     * @param saleId the sale
     * @param serialNumber the product's serial number
     */
    public void removeItemFromSale(int saleId, String serialNumber) {
        if (!dba.removeItemFromSale(saleId, serialNumber))
            view.print("Failed");
        else
            view.print("Done successfully!");
    }
}
