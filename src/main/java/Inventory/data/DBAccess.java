package Inventory.data;

import Inventory.business.BAL;
import Inventory.sharedClasses.Category;
import Inventory.sharedClasses.Product;
import Inventory.sharedClasses.Sale;
import Inventory.sharedClasses.Stock;

import java.sql.*;
import java.util.ArrayList;

public class DBAccess {

    private BAL logics;
    private Connection c;

    public DBAccess(BAL bal) {
        logics = bal;
        c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:Shop.db");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
        }
        System.out.println("Opened database successfully!");
    }

    /**
     * Gets a products name by its serial number
     *
     * @param serialNumber The products serial number
     * @return The products name
     */
    public String getProductName(String serialNumber) {
        String ans = "";
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ProductName FROM Products WHERE SerialNumber='" + serialNumber + "'");
            ans = rs.getString(1);
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Fetch products report from Products table.
     *
     * @return List of all the products
     */
    public ArrayList<Product> getAllProducts() {
        ArrayList<Product> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID");
            while (rs.next()) {
                Category cat = new Category(rs.getInt(4), rs.getString(5), rs.getInt(6));
                ans.add(new Product(rs.getString(1), rs.getString(2), rs.getString(3),
                        cat, rs.getInt(7), rs.getInt(8), rs.getInt(9)));
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Fetch inventory report from Inventory table.
     *
     * @return List of all the products
     */
    public ArrayList<Stock> getAllInventory() {
        ArrayList<Stock> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT I.SerialNumber, ProductName, Location, Quantity, DefectiveAmount, " +
                    "MinimalQuantity FROM Inventory AS I JOIN Products AS P ON I.SerialNumber=P.SerialNumber");
            while (rs.next()) {
                Stock stock = new Stock(rs.getString(1), rs.getString(3), rs.getInt(4),
                        rs.getInt(5), rs.getInt(6));
                stock.setProductName(rs.getString(2));
                ans.add(stock);
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Fetch products report from Products table of a given category by name.
     *
     * @return List of all the products of the given category
     */
    public ArrayList<Product> getAllProducts(String catName) {
        ArrayList<Product> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID " +
                    "WHERE Categories.CategoryName='" + catName + "'");
            while (rs.next()) {
                Category cat = new Category(rs.getInt(4), rs.getString(5), rs.getInt(6));
                ans.add(new Product(rs.getString(1), rs.getString(2), rs.getString(3),
                        cat, rs.getInt(7), rs.getInt(8), rs.getInt(9)));
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Fetch products report from Products table of a given category by name.
     *
     * @return List of all the products of the given category
     */
    public ArrayList<Product> getAllProducts(int catId) {
        ArrayList<Product> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID " +
                    "WHERE Categories.ID=" + catId);
            while (rs.next()) {
                Category cat = new Category(rs.getInt(4), rs.getString(5), rs.getInt(6));
                ans.add(new Product(rs.getString(1), rs.getString(2), rs.getString(3),
                        cat, rs.getInt(7), rs.getInt(8), rs.getInt(9)));
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Insert new sale to all products of a given category
     *
     * @param sale    The sale
     * @param catName The category
     * @return True if succeeded
     */
    public boolean addNewSaleByCategory(Sale sale, String catName) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SerialNumber, SellingPrice " +
                    "FROM Products JOIN Categories ON Category=ID " +
                    "WHERE CategoryName='" + catName + "'");
            while (rs.next()) {
                stmt.execute("INSERT INTO " + "ItemsOnSale" +
                        "VALUES(" + sale.getSaleId() + "," + rs.getString(1) + ")");
                stmt.execute("UPDATE Products " +
                        "SET SellingPrice=" + rs.getInt(2) * (1/100)*(100 - sale.getPercentOff()));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Insert new sale to a specific product
     *
     * @return True if succeeded
     */
    public boolean addNewSaleByProduct(Sale sale, int productSerialNumber) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.execute("INSERT INTO ItemsOnSale " +
                    "VALUES(" + sale.getSaleId() + ", '" + productSerialNumber + "')");
            stmt.execute("UPDATE Products " +
                    "SET SellingPrice=" +
                    stmt.executeQuery("SELECT SellingPrice FROM Products WHERE SerialNumber='"
                            + productSerialNumber + "'").getInt(1) *(100 - sale.getPercentOff())/100);

        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Fetch defectives products report
     *
     * @return List of all defectives products
     */
    public ArrayList<Stock> getAllDefectives() {
        ArrayList<Stock> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Inventory WHERE DefectiveAmount>0");
            while (rs.next()) {
                ans.add(new Stock(rs.getString(1), rs.getString(2),
                        rs.getInt(3), rs.getInt(4), rs.getInt(5)));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    public boolean insertSale(Sale sale) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.execute("INSERT INTO Sales " +
                    "VALUES(NULL, '" + sale.getStartDate() + "', '" + sale.getEndDate() + "', " + sale.getPercentOff() + ")");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean insertProduct(Product p) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.execute("INSERT INTO Products " +
                    "VALUES('" + p.getSerialNumber() + "', '" + p.getName() + "', '" + p.getManufacturer() + "', " + p.getCategory() + ", " +
                    p.getSize() + ", " + p.getCost() + ", " + p.getSellingPrice() + ")");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean insertCategory(Category cat) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.execute("INSERT INTO Categories " +
                    "VALUES(NULL, '" + cat.getName() + "', " + cat.getFatherCategory() + ")");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean updateSaleField( String field, String fieldValue, int saleId) {
        SaleFields parsedField = SaleFields.parse( field );
        if(parsedField == null) throw new RuntimeException("Wrong sale field");

        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("UPDATE Sales " +
                    "SET" + parsedField + "=" + fieldValue +
                    "WHERE Id=" + saleId );
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return true;
        //TODO
    }

    public boolean updateProductField(String field, String fieldValue, int serialNumber) {
        ProductFields parsedField = ProductFields.parse( field );
        if(parsedField == null) throw new RuntimeException("Wrong sale field");

        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("UPDATE Products" +
                    "SET" +parsedField+ "=" + fieldValue +
                    "WHERE SerialNumber=" + serialNumber);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return true;
        //TODO
    }

    public boolean updateCategoryField(String field, String fieldValue, int id) {
        CategoryFields parsedField = CategoryFields.parse( field );
        if(parsedField == null) throw new RuntimeException("Wrong sale field");

        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("UPDATE Categories" +
                                    "SET" +parsedField+ "=" + fieldValue +
                                    "WHERE ID=" + id);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return true;
        //TODO
    }

    public boolean removeCategory(int catID) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM Categories " +
                    "WHERE ID=" + catID);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean removeProduct(String serialNum) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM Products " +
                    "WHERE SerialNumber='" + serialNum + "'");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean removeSale(int saleId) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM Sales " +
                    "WHERE Id=" + saleId);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean addTriger() {
        //TODO
        return true;
    }

    /**
     * Find category by its name
     *
     * @param category
     * @return The category ID. if not found, return 0
     */
    public int findCategory(String category) {
        int newId;
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ID FROM Categories WHERE CategoryName='" + category + "'");
            newId = rs.getInt(1);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return 0;
        }
        return newId;
    }

    /**
     * Add new category to Categories table
     *
     * @param category the category's name
     * @return Its new ID, 0 if failed
     */
    public int addNewCategory(String category) {
        int newId;
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("INSERT INTO Categories " +
                    "VALUES (" + category + ", " + "NULL)");
            newId = findCategory(category);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return 0;
        }
        return newId;
    }

    /**
     * Sets product's category by its serial number
     *
     * @param serialNumber
     * @param categoryId
     * @return true if succeeded, else false
     */
    public boolean setProductsCategoryBySerialNumber(String serialNumber, int categoryId) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("UPDATE Products " +
                    "SET Category=" + categoryId + " WHERE SerialNumber='" + serialNumber + "'");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Sets product's category by its name
     *
     * @param name
     * @param categoryId
     * @return true if succeeded, else false
     */
    public boolean setProductsCategoryByName(String name, int categoryId) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("UPDATE Products " +
                    "SET Category=" + categoryId + " WHERE ProductName='" + name + "'");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Get all the sales
     * @return List of all sales in the DB
     */
    public ArrayList<Sale> getAllSales() {
        ArrayList<Sale> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Sales");
            while (rs.next()) {
                Sale sale = new Sale(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getInt(4));
                ans.add(sale);
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Get all current shortages
     * @return List of all shortages
     */
    public ArrayList<Stock> getLowQuantity() {
        ArrayList<Stock> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Inventory WHERE (Quantity-DefectiveAmount)<MinimalQuantity");
            while (rs.next()) {
                ans.add(new Stock(rs.getString(1), rs.getString(2),
                        rs.getInt(3), rs.getInt(4), rs.getInt(5)));
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Get all categories
     * @return List of all categories
     */
    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Categories");
            while (rs.next()) {
                Category cat = new Category(rs.getInt(1), rs.getString(2),
                        rs.getInt(3));
                if (cat.getFatherCategory() != 0)
                    cat.setFatherCategoryName(getCategoryName(cat.getFatherCategory()));
                ans.add(cat);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    private String getCategoryName(int fatherCategory) {
        String ans;
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Categories WHERE ID=" + fatherCategory);
            ans = rs.getString(2);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    public boolean updateStock(Stock stock) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("UPDATE Inventory " +
                    "SET Quantity=" + stock.getQuantity() + ", DefectiveAmount=" + stock.getDefectives() +
                    " WHERE SerialNumber='" + stock.getSerialNumber() + "' AND Location='" + stock.getLocation() + "'");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * puts item by given serial number on an existing sale
     * @param serialNumber product's serial number
     * @param saleId the sale id
     * @return true if succeeded, else false
     */
    public boolean addItemToSaleByProduct(String serialNumber, int saleId) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("INSERT INTO ItemsOnSale " +
                    "VALUES (" + saleId + ", '" + serialNumber + "')");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch from db all items on a given sale
     * @param saleId the sale's id
     * @return list of all the products on the sale
     */
    public ArrayList<Product> getAllItemsOnSale(int saleId) {
        ArrayList<Product> ans = new ArrayList();
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ProductId FROM ItemsOnSale WHERE SaleId= " + saleId);
            while (rs.next()) {
                String serialNumber = rs.getString(1);
                Product p = getProductDetails(serialNumber);
                if (p != null)
                    ans.add(p);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    public Product getProductDetails(String serialNum) {
        Product ans = null;
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID" +
                            " WHERE SerialNumber='" + serialNum + "'");
            Category cat = new Category(rs.getInt(4), rs.getString(5), rs.getInt(6));
            while (rs.next()) {
                ans = new Product(rs.getString(1), rs.getString(2), rs.getString(3),
                        cat, rs.getInt(7), rs.getInt(8), rs.getInt(9));
            }
            stmt.close();
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());
            return null;
        }
        return ans;
    }

    /**
     * Remove product from an existing sale
     * @param saleId the sale
     * @param serialNumber the product's serial number
     */
    public boolean removeItemFromSale(int saleId, String serialNumber) {
        Statement stmt;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM ItemsOnSale " +
                    "WHERE SaleId=" + saleId + " AND ProductId='" + serialNumber + "'");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Returns the minimal quantity of a product in a location
     * @param serialNumber the product's serial number
     * @param location the location
     * @return the minimal quantity
     */
    public int getProductMinimalQuantity(String serialNumber, String location) {
        int ans = -1;
        Statement stmt;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT MinimalQuantity FROM Inventory " +
                    "WHERE SerialNumber='" + serialNumber + "' AND Location='" + location + "'");
            while (rs.next()) {
                ans = rs.getInt(1);
            }
            stmt.close();
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());
            return -1;
        }
        return ans;
    }
}


/*package*/ enum CategoryFields{
    //ID,
    CategoryName,
    FatherCategory;

    /*package*/ static CategoryFields parse(String field){
        field = field.toLowerCase();

        if(field.equals("category name")|(field.equals( "categoryname")))
                return CategoryFields.CategoryName;
        if(field.equals("father category")|(field.equals( "fathercategory")))
                return CategoryFields.FatherCategory;
            return null;
    }
}

/*package*/ enum ProductFields{
    //SerialNumber,
    Name,
    Manufacturer,
    Category,
    Size,
    Cost,
    SellingPrice;

    /*package*/ static ProductFields parse(String field){
        field = field.toLowerCase();
        if(field.equals("name"))
                return ProductFields.Name;
        if(field.equals("manufacturer"))
                return ProductFields.Manufacturer;
        if(field.equals("category"))
                return ProductFields.Category;
        if(field.equals("size"))
                return ProductFields.Size;
        if(field.equals("cost"))
                return ProductFields.Cost;
        if(field.equals("selling price") | (field.equals("sellingprice")))
                return ProductFields.SellingPrice;
        return null;
    }
}

/*package*/ enum SaleFields{
    //Id,
    StartDate,
    EndDate,
    PercentOff;

    /*package*/ static SaleFields parse(String field){
        field = field.toLowerCase();
        if(field.equals("start date") | (field.equals("startdate")))
                return SaleFields.StartDate;

        if(field.equals("end date") | (field.equals("enddate")))
                return SaleFields.EndDate;

        if(field.equals("percent off")| (field.equals("percentoff")))
                return SaleFields.PercentOff;
        return null;
    }
}
