package Inventory.sharedClasses;

/**
 * Created by Shahar on 28/03/2017.
 */
public class Product {

    private String serialNumber;
    private String name;
    private String manufacturer;
    private Category category;
    private int size;
    private int cost;
    private int sellingPrice;

    public Product(String serialNumber, String name, String manufacturer, Category category, int size, int cost, int sellingPrice) {
        this.serialNumber = serialNumber;
        this.name = name;
        this.manufacturer = manufacturer;
        this.category = category;
        this.size = size;
        this.cost = cost;
        this.sellingPrice = sellingPrice;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Category getCategory() {
        return category;
    }

    public int getSize() {
        return size;
    }

    public int getCost() {
        return cost;
    }

    public int getSellingPrice() {
        return sellingPrice;
    }

    @Override
    public String toString() {
        return "Product{" +
                "serialNumber='" + serialNumber + '\'' +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", category=" + category.getName() +
                ", size=" + size +
                ", cost=" + cost +
                ", sellingPrice=" + sellingPrice +
                '}';
    }
}
