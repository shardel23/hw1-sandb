package Inventory.sharedClasses;

/**
 * Created by Shahar on 28/03/2017.
 */
public class Sale {

    private int saleId;
    private String startDate;
    private String endDate;
    private int percentOff;

    public Sale(int saleId, String startDate, String endDate, int percentOff) {
        this.saleId = saleId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.percentOff = percentOff;
    }

    public Sale(String startDate, String endDate, int percentOff) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.percentOff = percentOff;
    }

    public int getSaleId() {
        return saleId;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getPercentOff() {
        return percentOff;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "saleId=" + saleId +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", percentOff=" + percentOff +
                '}';
    }
}
