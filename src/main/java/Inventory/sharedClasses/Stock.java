package Inventory.sharedClasses;

/**
 * Created by Shahar on 30/03/2017.
 */
public class Stock {

    private String serialNumber;
    private String productName;
    private String location;
    private int quantity;
    private int defectives;
    private int minimal;

    public Stock(String serialNumber, String location, int quantity, int defectives, int minimal) {
        this.serialNumber = serialNumber;
        productName = "";
        this.location = location;
        this.quantity = quantity;
        this.defectives = defectives;
        this.minimal = minimal;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String name) {
        productName = name;
    }

    public String getLocation() {
        return location;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getDefectives() {
        return defectives;
    }

    public int getMinimal() {
        return minimal;
    }

    @Override
    public String toString() {
        String ans = "";
        ans += "Stock{" +
                "serialNumber=" + serialNumber;
        if (productName != "")
            ans += ", name='" + productName + '\'';
        ans += ", location='" + location + '\'' +
                ", quantity=" + quantity +
                ", defectives=" + defectives +
                ", minimal=" + minimal +
                '}';
        return ans;
    }
}
