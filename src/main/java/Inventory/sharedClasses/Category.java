package Inventory.sharedClasses;

/**
 * Created by Shahar on 28/03/2017.
 */
public class Category {

    private int id;
    private String name;
    private int fatherCategory;
    private String fatherCategoryName;

    public Category(int id, String name, int fatherCategory) {
        this.id = id;
        this.name = name;
        this.fatherCategory = fatherCategory;
    }

    public Category(int id, String name, int fatherCategory, String fatherCategoryName) {
        this.id = id;
        this.name = name;
        this.fatherCategory = fatherCategory;
        this.fatherCategoryName = fatherCategoryName;
    }

    public Category(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFatherCategory() {
        return fatherCategory;
    }

    public String getFatherCategoryName() {
        return fatherCategoryName;
    }

    public void setFatherCategoryName(String name) {
        fatherCategoryName = name;
    }

    @Override
    public String toString() {
        String ans = "";
        ans += "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", fatherCategory=" + fatherCategory;
        if (fatherCategoryName != "")
            ans += ", fatherCategoryName='" + fatherCategoryName + '\'';
        ans += '}';
        return ans;
    }
}
