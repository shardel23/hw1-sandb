package Inventory.presentation;

import Inventory.business.BAL;
import Inventory.sharedClasses.Category;
import Inventory.sharedClasses.Product;
import Inventory.sharedClasses.Sale;
import Inventory.sharedClasses.Stock;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Shahar on 29/03/2017.
 */
public class CLI {

    private BAL logics;
    private Scanner scan;

    public CLI() {
        logics = new BAL(this);
        scan = new Scanner(System.in);
    }

    /**
     * Main loop for the app
     */
    public void run() {
        System.out.println("Welcome to the shop!\n");
        while (true) {
            showMenu();
            int choice = scan.nextInt(); //Assumes input is in correct format
            switch (choice) {
                case 1: {
                    logics.getAllProducts();
                    break;
                }
                case 2: {
                    String catName = "default";
                    ArrayList<String> categories = new ArrayList<String>();
                    System.out.println("Type the name of the categories. When done input 'd'");
                    while (!catName.equals("d")) {
                        catName = scan.next();
                        if (!catName.equals("d"))
                            categories.add(catName);
                    }
                    logics.getAllProducts(categories);
                    break;
                }
                case 3: {
                    logics.getAllDeffectives();
                    break;
                }
                case 4: {
                    logics.getLowQuantity();
                    break;
                }
                case 5: {
                    System.out.println("Choose by 1)Serial number 2)Product's name");
                    choice = scan.nextInt();
                    String data;
                    if (choice == 1) {
                        System.out.println("Type product's serial number...");
                    } else {
                        System.out.println("Type product's name...");
                    }
                    data = scan.next();
                    System.out.println("Type category name...");
                    String category = scan.next();
                    logics.setProductsCategory(choice, data, category);
                    break;
                }
                case 6: {
                    logics.getAllCategories();
                    break;
                }
                case 7: {
                    System.out.println("Type start date...");
                    String startDate = scan.next();
                    System.out.println("Type end date...");
                    String endDate = scan.next();
                    System.out.println("Type percentage off...");
                    int percentOff = scan.nextInt();
                    Sale newSale = new Sale(startDate, endDate, percentOff);
                    logics.newSale(newSale);
                    break;
                }
                case 8: {
                    System.out.println("Type sale ID...");
                    int saleID = scan.nextInt();
                    logics.deleteSale(saleID);
                    break;
                }
                case 9: {
                    System.out.println("Type serial number...");
                    String serial = scan.next();
                    System.out.println("Type product's name...");
                    String name = scan.next();
                    System.out.println("Type manufacture's name...");
                    String manuf = scan.next();
                    System.out.println("Type size...");
                    int size = scan.nextInt();
                    System.out.println("Type cost...");
                    int cost = scan.nextInt();
                    System.out.println("Type selling price...");
                    int price = scan.nextInt();
                    Product newProduct = new Product(serial, name, manuf, null, size, cost, price);
                    logics.newProduct(newProduct);
                    break;
                }
                case 10: {
                    System.out.println("Type product's serial number...");
                    String productName = scan.next();
                    logics.deleteProduct(productName);
                    break;
                }
                case 11: {
                    System.out.println("Type category's name...");
                    String name = scan.next();
                    Category newCat = new Category(name);
                    logics.newCategory(newCat);
                    break;
                }
                case 12: {
                    System.out.println("Type category's ID...");
                    int catID = scan.nextInt();
                    logics.deleteCategory(catID);
                    break;
                }
                case 13: {
                    System.out.println("Type product's serial number...");
                    String serial = scan.next();
                    System.out.println("Type product's Location...");
                    String location = scan.next();
                    System.out.println("Type new quantity...");
                    int quantity = scan.nextInt();
                    System.out.println("Type new defectives amount...");
                    int defectives = scan.nextInt();
                    Stock stock = new Stock(serial, location, quantity, defectives, 0);
                    logics.updateInventory(stock);
                    break;
                }
                case 14: {
                    logics.getAllInventory();
                    break;
                }
                case 15: {
                    logics.getAllSales();
                    break;
                }
                case 16: {
                    System.out.println("Enter requested field:\n");
                    String field = scan.next();
                    System.out.println("Enter requested value:\n");
                    String value = scan.next();
                    System.out.println("Enter sale id:");
                    int id = scan.nextInt();
                    logics.editSaleField( field, value , id);
                }
                case 17: {
                    System.out.println("Enter requested field:\n");
                    String field = scan.next();
                    System.out.println("Enter requested value:\n");
                    String value = scan.next();
                    System.out.println("Enter product serial number:");
                    int serial = scan.nextInt();
                    logics.editProductField( field, value, serial );
                }
                case 18: {
                    System.out.println("Enter requested field:\n");
                    String field = scan.next();
                    System.out.println("Enter requested value:\n");
                    String value = scan.next();
                    System.out.println("Enter category id:");
                    int id = scan.nextInt();
                    logics.editCategoryField( field, value , id);
                }
                case 19: {
                    System.out.println("Choose by 1)Product's serial number 2)Category's ID");
                    choice = scan.nextInt();
                    String data;
                    if (choice == 1) {
                        System.out.println("Type product's serial number...");
                    } else {
                        System.out.println("Type Category's ID...");
                    }
                    data = scan.next();
                    System.out.println("Type Sale ID...");
                    int saleId = scan.nextInt();
                    logics.addItemsToSale(choice, data, saleId);
                    break;
                }
                case 20: {
                    System.out.println("Type Sale ID...");
                    int saleId = scan.nextInt();
                    logics.showAllItemsOnSale(saleId);
                    break;
                }
                case 21: {
                    System.out.println("Type Sale ID...");
                    int saleId = scan.nextInt();
                    System.out.println("Type product's serial number..");
                    String serialNumber = scan.next();
                    logics.removeItemFromSale(saleId, serialNumber);
                    break;
                }
                case 22: {
                    System.out.println("Goodbye!");
                    System.exit(0);
                }
            }
        }
    }

    /**
     * Prints the main menu
     */
    private void showMenu() {
        System.out.println("What would you like to do?\n" +
                "1) Products Report\n" +
                "2) Products Report (By Category)\n" +
                "3) Defectives Report\n" +
                "4) Shortage Report\n" +
                "5) Set category for product\n" +
                "6) Categories Report\n" +
                "7) Add new sale\n" +
                "8) Remove sale\n" +
                "9) Add new product\n" +
                "10) Remove product\n" +
                "11) Add new category\n" +
                "12) Remove category\n" +
                "13) Update inventory's amounts\n" +
                "14) Inventory Report\n" +
                "15) Sales Report\n" +
                "16) Edit sale details\n" +
                "17) Edit product details\n" +
                "18) Edit category\n" +
                "19) Add items to a sale\n" +
                "20) View items on a sale\n" +
                "21) Remove item from a sale\n" +
                "22) Exit");
    }

    /**
     * Prints a given String
     * @param str
     */
    public void print(String str) {
        System.out.println(str);
    }

    /**
     * Prints the toString method of every element in the list
     * @param list
     */
    public void print(ArrayList<? extends Object> list) {
        for (Object item : list) {
            System.out.println(item.toString());
        }
    }


}
