import Inventory.business.BAL;
import Inventory.data.DBAccess;
import Inventory.presentation.CLI;
import Inventory.sharedClasses.Category;
import Inventory.sharedClasses.Product;
import Inventory.sharedClasses.Sale;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shahar on 02/04/2017.
 */
public class DBAccessTest {


    static DBAccess db;
    boolean set = false;
    Category category = new Category(5, "Milks", 0);
    private DBAccess getDB(){
        if(set){
            return db;
        }
        set = true;
        return db = new DBAccess( new BAL( new CLI() ) );
    }
    @Test
    public void getProductNameTest(){
        db = getDB();
        String serial = "12345";
        String name = "lactose free milk";
        Product p = new Product(serial, name, "test", category, 1,1,1);
        db.insertProduct(p);
        String temp = db.getProductName( serial );
        assertEquals(temp, name);
    }
    @Test
    public void addNewSaleByProductTest(){
        Sale sale = new Sale(45679, "01/05/2017","01/06/2017",35);
        String name = "something";
        Product p = new Product("4689", name , "test", category, 1,100,1);
        db.insertProduct(p);
        db.addNewSaleByProduct(sale,4589);
        ArrayList<Product> array = db.getAllProducts();
        for(Product p1 : array){
            if(p1.getName().equals( name )) assertEquals(45, p1.getSellingPrice());
        }
    }
    @Test
    public void addNewSaleByCategoryTest(){
        Sale sale = new Sale(45678, "01/05/2017","01/06/2017",55);
        String name = "something";
        Product p = new Product("4589", name , "test", category, 1,100,1);
        db.addNewSaleByCategory(sale,"Milks");
        ArrayList<Product> array = db.getAllProducts();
        for(Product p1 : array){
            if(p1.getName().equals( name )) assertEquals(45, p1.getSellingPrice());
        }
    }
    @Test
    public void insertProductTest(){
        String name = "test2";
        Product p = new Product("4589", name ,"insertMilk", category, 1,100,100);
        db.insertProduct( p );
        ArrayList<Product> array = db.getAllProducts();
        for(Product p1 : array){
            if(p1.getName().equals( name )) assertEquals("insertMilk", p1.getName());
        }
    }
    @Test
    public void insertCategoryTest(){
        Category category = new Category(7, "Milks2", 0);
        db.insertCategory( category );
        ArrayList<Category> array = db.getAllCategories();
        for(Category c: array){
            assertTrue(c.getName().equals("Milks2") & c.getId() == 7);
        }

    }
    @Test
    public void insertSaleTest(){
        Sale sale = new Sale(45688, "01/05/2017","01/06/2017",55);
        db.insertSale( sale );
        ArrayList<Sale> array = db.getAllSales();
        for(Sale s: array){
            assertTrue(s.getSaleId() == 45688);
        }
    }
    @Test
    public void updateSaleFieldTest(){
        Sale sale = new Sale(46688, "01/05/2017","01/06/2017",55);
        db.updateSaleField("percentOff","50",46688);
        ArrayList<Sale> array = db.getAllSales();
        for(Sale s: array){
            assertTrue((s.getSaleId() == 45688 & s.getPercentOff()==50));
        }
    }
    @Test
    public void updateProductFieldTest(){
        Product p = new Product("4789", "milk2" ,"Milk5", category, 1,100,100);
        db.updateProductField("sellingPrice","90",4789);
        ArrayList<Product> array = db.getAllProducts();
        for(Product p1 : array){
            assertTrue(p1.getName().equals( "milk2" ) & p1.getSellingPrice()==90);
        }
    }
    @Test
    public void removeCategoryTest(){
        Category category = new Category(6, "Milks4", 0);
        db.insertCategory( category );
        db.removeCategory(6);
        ArrayList<Category> array = db.getAllCategories();
        int wrong = 0;
        for(Category c: array){
            if(c.getName().equals("Milks4") & c.getId()==6)
                wrong++;
        }
        assertTrue(wrong == 0);
    }

    @Test
    public void removeProductTest(){
        Product p = new Product("7789", "milk8" ,"Milk5", category, 1,100,100);
        db.insertProduct( p );
        db.removeProduct( "7789" );
        ArrayList<Product> array = db.getAllProducts();
        int wrong = 0;
        for(Product p1 : array){
            if(p1.getName().equals( "milk8" ) & p1.getSerialNumber().equals(7789))
                wrong++;
        }
        assertTrue(wrong == 0);
    }

}