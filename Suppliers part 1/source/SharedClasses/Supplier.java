package SharedClasses;

import java.util.LinkedList;

public class Supplier {
	
	private String id;
	private String bankAccount;
	private String termsOfPayment;
	private LinkedList<Contact> contacts;
	private SupplyTerms terms;
	private LinkedList<Item> items;
	private LinkedList<Discount> discounts;
	
	public Supplier(String id, String bankAccount, String termsOfPayment, LinkedList<Contact> contacts, SupplyTerms terms, LinkedList<Item> items){
		this.id = id;
		this.bankAccount = bankAccount;
		this.termsOfPayment = termsOfPayment;
		this.contacts = contacts;
		this.terms = terms;
		this.items = items;
		this.discounts = new LinkedList<>();
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTermsOfPayment() {
		return termsOfPayment;
	}

	public void setTermsOfPayment(String termsOfPayment) {
		this.termsOfPayment = termsOfPayment;
	}

	public LinkedList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(LinkedList<Contact> contacts) {
		this.contacts = contacts;
	}

	public SupplyTerms getTerms() {
		return terms;
	}

	public void setTerms(SupplyTerms terms) {
		this.terms = terms;
	}

	public LinkedList<Item> getItems() {
		return items;
	}

	public void setItems(LinkedList<Item> items) {
		this.items = items;
	}

	public LinkedList<Discount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(LinkedList<Discount> discounts) {
		this.discounts = discounts;
	}
	
	
}
