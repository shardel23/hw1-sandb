package SharedClasses;

public class Discount {
	
	private String catalogNumber;
	private int quantity;
	private double discount;
	
	public Discount(String catalogNumber, int quantity, double discount){
		this.catalogNumber = catalogNumber;
		this.quantity = quantity;
		this.discount = discount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getCatalogNumber() {
		return catalogNumber;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}
	
}
