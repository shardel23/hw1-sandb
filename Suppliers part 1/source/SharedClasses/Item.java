package SharedClasses;

public class Item {

	private String catalogNumber;
	private double price;
	private String name;
	
	public Item(String catalogNumber, double price, String name){
		this.catalogNumber = catalogNumber;
		this.name = name;
		this.price = price;
	}

	public String getCatalogNumber() {
		return catalogNumber;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return ("Catalog Number: " + catalogNumber + " , Item Name: " + name + " , Price: " + price);
	}
}
