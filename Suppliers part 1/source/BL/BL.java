package BL;
import java.util.LinkedList;

import DAL.DAL;
import SharedClasses.*;

public class BL {

    private DAL dal;

    public BL() {
        dal = new DAL();
    }

    public boolean isNumber(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean legalName(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ' && s.charAt(i) != '\n' && !Character.isLetter(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean isPositiveDouble(String s) {
        boolean ans = false;
        try {
            double d = Double.parseDouble(s);
            if (d < 0) {
                ans = false;
            } else {
                ans = true;
            }
        } catch (NumberFormatException e) {
            ans = false;
        }
        return ans;
    }


    public boolean supplierExists(String id) {
        return dal.supplierExists(id);
    }

    public boolean insetToSuppliers(Supplier supplier) {
        return dal.insertToSuppliers(supplier);
    }

    public boolean updateSupplyTerms(String supplier_id, SupplyTerms terms) {
        return dal.updateSupplyTerms(supplier_id, terms);
    }

    public LinkedList<Item> getSupplierItems(String supplier_id) {
        return dal.getSupplierItems(supplier_id);
    }

    public boolean deleteContact(String supplier_id, String phoneNum) {
        return dal.deleteContact(supplier_id, phoneNum);
    }

    public boolean ItemExists(String supplierID, String catalogNum) {
        return dal.itemExists(supplierID, catalogNum);
    }

    public boolean deleteSupplier(String id) {
        return dal.deleteSupplier(id);
    }

    public boolean addItem(String id, Item item) {
        return dal.insertToItemsInSupplier(id, item);
    }

    public boolean deleteItem(String id, String catalogNum) {
        return dal.deleteItemOfSupplier(id, catalogNum);
    }

    public boolean addContact(String id, Contact contact, boolean shouldInsertToContacts) {
        return dal.insertToContacts(id, contact, shouldInsertToContacts);
    }

    public boolean contactExistsWithSupp(String supplierID, String phoneNum) {
        return dal.contactExistsWithSupp(supplierID, phoneNum);
    }

    public boolean insertToDiscounts(String supplierID, Discount discount) {
        return dal.insertToDiscounts(supplierID, discount);
    }

    public boolean deleteFromDiscounts(String supplierId, String catalog_number, int quantity){
        return dal.deleteDiscount(supplierId, catalog_number, quantity);
    }

    public boolean contactExistInContacts(String phoneNum) {
        return dal.contactExistsInContacts(phoneNum);
    }

	public void exit() {
		dal.close();
	}
}
