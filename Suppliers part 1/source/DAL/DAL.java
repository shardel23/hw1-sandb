package DAL;

import SharedClasses.*;

import java.sql.*;
import java.util.LinkedList;


public class DAL {
    Connection c = null;
    Statement stmt = null;


    //Constructor
    public DAL() {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:Shop.db");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Cannot connect to DB. Exiting...");
            System.exit(0);
        }
    }
    
    public boolean insertToSuppliers(Supplier supplier) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Suppliers (id, bank_account, payment, supply_terms) VALUES ('" +
                    supplier.getId() + "', '" + supplier.getBankAccount() + "', '" + supplier.getTermsOfPayment() + "', '" + supplier.getTerms() + "');";
            stmt.executeUpdate(sql);
            for (Contact c : supplier.getContacts()) {
                insertToContacts(supplier.getId(), c, contactExistsInContacts(c.getPhone()));
            }
            for (Item item : supplier.getItems()) {
                insertToItemsInSupplier(supplier.getId(), item);
            }
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertToContacts(String id, Contact contact, boolean shouldEnterToContacts) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Contacts_Of_Suppliers (supplier_id, phone_number) VALUES ('" +
                    id + "', '" + contact.getPhone() + "');";
            stmt.executeUpdate(sql);
            if (!shouldEnterToContacts) {
                sql = "INSERT INTO Contacts (name, phone_number) VALUES ('" +
                        contact.getName() + "', '" + contact.getPhone() + "');";
                stmt.executeUpdate(sql);
            } else {
                sql = "UPDATE Contacts SET name = '" + contact.getName() + "' WHERE phone_number = '" + contact.getPhone() + "';";
                stmt.executeUpdate(sql);
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertToItemsInSupplier(String supplier_id, Item item) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Items_In_Contract (supplier_id, catalog_number, name, price) VALUES ('" +
                    supplier_id + "', '" + item.getCatalogNumber() + "', '" + item.getName() + "', " + item.getPrice() + ");";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteItemOfSupplier(String supplier_id, String item) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Items_In_Contract WHERE supplier_id = " + supplier_id + " AND catalog_number = " + item + ";";
            stmt.executeUpdate(sql);
            sql = "DELETE FROM Discounts WHERE supplier_id = " + supplier_id + " AND catalog_number = " + item + ";";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteContact(String supplier_id, String phoneNum) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Contacts_Of_Suppliers WHERE supplier_id = '" + supplier_id + "' AND phone_number = '" + phoneNum + "';";
            stmt.executeUpdate(sql);
            sql = "SELECT COUNT (*) > 0 FROM Contacts_Of_Suppliers WHERE phone_number = '" + phoneNum + "' ;";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                boolean ans = rs.getBoolean(1);
                if (!ans) {
                    sql = "DELETE FROM Contacts WHERE phone_number = '" + phoneNum + "';";
                    stmt.executeUpdate(sql);
                }
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteSupplier(String id) {
        try {
            stmt = c.createStatement();
            //sql = "DELETE FROM Contacts_Of_Suppliers WHERE supplier_id = '" + id + "';";
            //stmt.executeUpdate(sql);
            String sql = "SELECT phone_number FROM Contacts_Of_Suppliers WHERE supplier_id = '" + id + "';";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()){
                String phoneNum = rs.getString(1);
                this.deleteContact(id, phoneNum);
            }
            sql = "DELETE FROM Discounts WHERE supplier_id = '" + id + "';";
            stmt.executeUpdate(sql);
            sql = "DELETE FROM Items_In_Contract WHERE supplier_id = '" + id + "';";
            stmt.executeUpdate(sql);
            sql = "DELETE FROM Suppliers WHERE id = '" + id + "';";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateSupplyTerms(String supplier_id, SupplyTerms terms) {
        try {
            stmt = c.createStatement();
            String sql = "UPDATE Suppliers SET supply_terms = '" + terms + "' WHERE id = '" + supplier_id + "';";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public LinkedList<Item> getSupplierItems(String supplier_id) {
        LinkedList<Item> ans = new LinkedList<Item>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Items_In_Contract WHERE supplier_id = " + supplier_id + ";";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ans.add(new Item(rs.getString(2), rs.getDouble(4), rs.getString(3)));
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public boolean supplierExists(String id) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Suppliers WHERE id = " + id + ";";
            ResultSet rs = stmt.executeQuery(sql);
            if (!rs.next()) {
                return false;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean itemExists(String supplierId, String catalog_num) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT(*) FROM Items_In_Contract WHERE supplier_id = '" + supplierId + "' AND catalog_number = '" + catalog_num + "';";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                boolean ans = rs.getBoolean(1);
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean contactExistsWithSupp(String supplierID, String phoneNum) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT (*) > 0 FROM Contacts_Of_Suppliers WHERE supplier_id = '" + supplierID + "' AND phone_number = '" + phoneNum + "';";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                boolean ans = rs.getBoolean(1);
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertToDiscounts(String supplierId, Discount discount) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Discounts (supplier_id, catalog_number, quanitity, discount) VALUES ('" +
                    supplierId + "', '" + discount.getCatalogNumber() + "', " + discount.getQuantity() + ", " + discount.getDiscount() + ");";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteDiscount(String supplierId, String catalog_number, int quantity) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Discounts WHERE supplier_id = " + supplierId + " AND quanitity = " + quantity + " AND catalog_number = " + catalog_number + ";";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean contactExistsInContacts(String phoneNum) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT (*) FROM Contacts WHERE phone_number = '" + phoneNum + "' ;";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                boolean ans = rs.getBoolean(1);
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
		}
	}
}