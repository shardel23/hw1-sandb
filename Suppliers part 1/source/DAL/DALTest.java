package DAL;

import SharedClasses.*;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class DALTest {
    private DAL dal;

    @org.junit.Before
    public void setUp() throws Exception {
        dal = new DAL();
        dal.insertToSuppliers(new Supplier("-1", "0", "Cash", new LinkedList<Contact>(), SupplyTerms.OnOrder, new LinkedList<Item>()));
    }
    
    @org.junit.After
    public void tearDown(){
    	dal.deleteSupplier("-1");
    	dal.close();
    }

    @org.junit.Test
    public void insertToSuppliers() throws Exception {
        dal.insertToSuppliers(new Supplier("-2", "0", "Cash", new LinkedList<Contact>(), SupplyTerms.OnOrder, new LinkedList<Item>()));
        assertTrue(dal.supplierExists("-2"));
        dal.deleteSupplier("-2");
    }

    @org.junit.Test
    public void insertToContacts() throws Exception {
        dal.insertToContacts("-1", new Contact("John Doe", "0"), true);
        assertTrue(dal.contactExistsWithSupp("-1", "0"));
    }

    @org.junit.Test
    public void insertToItemsInSupplier() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 0, "0"));
        assertTrue(dal.itemExists("-1", "0"));
        dal.deleteItemOfSupplier("-1", "0");
    }

    @org.junit.Test
    public void deleteItemOfSupplier() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 0, "0"));
        dal.deleteItemOfSupplier("-1", "0");
        assertFalse(dal.itemExists("-1", "0"));
    }

    @org.junit.Test
    public void deleteContact() throws Exception {
        dal.insertToContacts("-1", new Contact("John Doe", "0"), true);
        dal.deleteContact("-1", "0");
        assertFalse(dal.contactExistsInContacts("-1"));
    }

    @org.junit.Test
    public void deleteSupplier() throws Exception {
        dal.deleteSupplier("-1");
        assertFalse(dal.supplierExists("-1"));
        dal.insertToSuppliers(new Supplier("-1", "0", "Cash", new LinkedList<Contact>(), SupplyTerms.OnOrder, new LinkedList<Item>()));
    }

    @org.junit.Test
    public void getSupplierItems() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 0, "0"));
        assertEquals(dal.getSupplierItems("-1").size(), 1);
        dal.deleteItemOfSupplier("-1", "0");
    }

    @org.junit.Test
    public void supplierExists() throws Exception {
        assertTrue(dal.supplierExists("-1"));
    }

    @org.junit.Test
    public void itemExists() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 0, "0"));
        assertTrue(dal.itemExists("-1", "0"));
        dal.deleteItemOfSupplier("-1", "0");
        assertFalse(dal.itemExists("-1", "0"));
    }

    @org.junit.Test
    public void insertToDiscounts() throws Exception {
        dal.insertToItemsInSupplier("-1",new Item("0",12.12, "Test"));
        assertTrue(dal.insertToDiscounts("-1", new Discount("0",1,1)));
        dal.deleteDiscount("-1", "0", 1);

    }

}