package PL;
import java.util.LinkedList;
import java.util.Scanner;

import BL.BL;
import SharedClasses.*;

public class PL_Manager {
    private BL bl;
    private Scanner sc;

    public PL_Manager() {
        bl = new BL();
        sc = new Scanner(System.in);
    }

    public void main() {
        while (true) {
            boolean printOp = false;
            System.out.println("Welcome to Main Menu");
            System.out.println("Select an option:");
            System.out.println("1. Enter new supplier");
            System.out.println("2. Delete supplier");
            System.out.println("3. Add item to contract with supplier");
            System.out.println("4. Delete item from contact with supplier");
            System.out.println("5. Update supply term of a supplier");
            System.out.println("6. Show items in contract with supplier");
            System.out.println("7. Add contact to supplier");
            System.out.println("8. Delete contact of supplier");
            System.out.println("9. Add Discount of an item");
            System.out.println("10. Delete Discount of an item");
            System.out.println("11. Exit");
            String option = sc.nextLine();
            while (!printOp) {
                switch (option) {
                    case "1":
                        addSupplier();
                        printOp = true;
                        break;
                    case "2":
                        deleteSupplier();
                        printOp = true;
                        break;
                    case "3":
                        addItem();
                        printOp = true;
                        break;
                    case "4":
                        deleteItem();
                        printOp = true;
                        break;
                    case "5":
                        updateSupplyTerms();
                        printOp = true;
                        break;
                    case "6":
                        showItems();
                        printOp = true;
                        break;
                    case "7":
                        addContact();
                        printOp = true;
                        break;
                    case "8":
                        deleteContact();
                        printOp = true;
                        break;
                    case "9":
                        addDiscount();
                        printOp = true;
                        break;
                    case "10":
                        deleteDiscount();
                        printOp = true;
                        break;
                    case "11":
                    	bl.exit();
                        System.exit(0);
                    default:
                        System.out.println("invalid option, please try again");
                        option = sc.nextLine();
                }
            }
        }
    }

    private void addSupplier() {
        System.out.println("Enter new supplier's Id number:");
        boolean ret = false;
        String id = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(id)) {
                System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else if (bl.supplierExists(id)) {
                System.out.println("Id is in the system");
                System.out.println("try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        System.out.println("Enter bank acocunt:");
        String bankAccount = getNumber("Bank Account");
        if (bankAccount.equals("back")) {
            return;
        }
        System.out.println("Enter payement method:");
        String payment = sc.nextLine();
        while (payment.equals("") || payment.charAt(0) == ' ' || !bl.legalName(payment)) {
            System.out.println("Payement method cannot be empty, start with a space or contain characters that aren't letters.");
            System.out.println("try again or enter 'back' to return to go back");
            payment = sc.nextLine();
            if (payment.equals("back")) {
                return;
            }
        }
        SupplyTerms supplyTerms = getSupplyTerms();
        boolean addContact = true;
        LinkedList<Contact> contacts = new LinkedList<>();
        String op;
        boolean correctOp;
        while (addContact) {
            System.out.println("do you want to add a new contact to the supplier?");
            System.out.println("press 1 for 'yes', or press 2 for 'no'");
            op = sc.nextLine();
            correctOp = false;
            while (!correctOp) {
                switch (op) {
                    case "1":
                        Contact contact = getContact("");
                        if (contact != null) {
                            contacts.add(contact);
                        }
                        correctOp = true;
                        break;
                    case "2":
                        correctOp = true;
                        addContact = false;
                        break;
                    default:
                        System.out.println("invalid option, please try again");
                        op = sc.nextLine();
                }
            }
        }
        boolean addItem = true;
        LinkedList<Item> items = new LinkedList<>();
        while (addItem) {
            System.out.println("do you want to add a new item to the contract with the supplier?");
            System.out.println("press 1 for 'yes', or press 2 for 'no'");
            op = sc.nextLine();
            correctOp = false;
            while (!correctOp) {
                switch (op) {
                    case "1":
                        Item item = getItem("");
                        if (item != null) {
                            items.add(item);
                        }
                        correctOp = true;
                        break;
                    case "2":
                        correctOp = true;
                        addItem = false;
                        break;
                    default:
                        System.out.println("invalid option, please try again");
                        op = sc.nextLine();
                }
            }
        }
        boolean success = bl.insetToSuppliers(new Supplier(id, bankAccount, payment, contacts, supplyTerms, items));
        if (success) {
            System.out.println("The new supplier was added successfuly!");
        } else {
            System.out.println("Problem adding supplier");
        }
    }

    private void deleteSupplier() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        boolean success = bl.deleteSupplier(id);
        if (success) {
            System.out.println("The supplier was deleted successfully!");
        } else {
            System.out.println("Problem deleting supplier");
        }
    }

    private void addItem() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        Item item = getItem(id);
        if (item == null) {
            return;
        }
        if (bl.addItem(id, item)) {
            System.out.println("Item added successfully");
        } else {
            System.out.println("Error adding item");
        }
        return;
    }

    private void deleteItem() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        System.out.println("Please enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system");
            System.out.println("Try again or enter 'back' to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        if (bl.deleteItem(id, catalogNum)) {
            System.out.println("Item successfully deleted");
        } else {
            System.out.println("Problem deleting item");
        }
    }

    private void showItems() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        LinkedList<Item> items = bl.getSupplierItems(id);
        if (items == null) {
            return;
        }
        for (Item item : items) {
            System.out.println(item.toString());
        }
        System.out.println("");
    }

    private void updateSupplyTerms() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        SupplyTerms supplyTerms = getSupplyTerms();

        boolean success = bl.updateSupplyTerms(id, supplyTerms);
        if (success) {
            System.out.println("Supply terms were updated successfully!");
        } else {
            System.out.println("Problem updating supply terms");
        }
    }

    private void deleteContact() {
        String id = getSupplierId();
        System.out.println("Please enter contact's phone number:");
        String phoneNum = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(phoneNum)) {
                System.out.println("Phone number must be a positive number, please enter again or enter 'back' to go back");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return;
                }
            }
            if (!bl.contactExistsWithSupp(id, phoneNum)) {
                System.out.println("Contact doesn't exist, please try again or enter 'back' to go back.");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }

        }
        if(bl.deleteContact(id, phoneNum)){
            System.out.println("Contact was deleted successfully");
        } else {
            System.out.println("Error deleting contact");
        }
    }

    private String getSupplierId() {
        System.out.println("Enter supplier's Id number:");
        boolean ret = false;
        String id = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(id)) {
                System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return "back";
                }
            } else if (!bl.supplierExists(id)) {
                System.out.println("Id is not in the system");
                System.out.println("try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return "back";
                }
            } else {
                ret = true;
            }
        }
        return id;
    }

    private String getNumber(String s) {
        String get = sc.nextLine();
        while (!bl.isNumber(get)) {
            System.out.println(s + " must be a positive number, please enter again");
            System.out.println("try again or enter 'back' to go back");
            get = sc.nextLine();
            if (get.equals("back")) {
                return "back";
            }
        }
        return get;
    }

    private SupplyTerms getSupplyTerms() {
        System.out.println("Select an option for suppliying terms:");
        System.out.println("1. Transport on set days");
        System.out.println("2. Transport on order");
        System.out.println("3. Self transport");
        String op = sc.nextLine();
        SupplyTerms supplyTerms = null;
        boolean correctOp = false;
        while (!correctOp) {
            switch (op) {
                case "1":
                    supplyTerms = SupplyTerms.SupplyOnSetDays;
                    correctOp = true;
                    break;
                case "2":
                    supplyTerms = SupplyTerms.OnOrder;
                    correctOp = true;
                    break;
                case "3":
                    supplyTerms = SupplyTerms.SelfTransport;
                    correctOp = true;
                    break;
                default:
                    System.out.println("invalid option, please try again");
                    op = sc.nextLine();
            }
        }
        return supplyTerms;
    }

    private void addContact() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        Contact contact = getContact(id);
        if (contact == null) {
            return;
        }
        boolean changeNameInContacts = bl.contactExistInContacts(contact.getPhone());
        if (changeNameInContacts) {
            System.out.println("Contact with the same phone already exists, his name will be changed");
        }
        if (bl.addContact(id, contact, changeNameInContacts)) {
            System.out.println("Contact was added successfully");
        } else {
            System.out.println("Error adding contact");
        }
    }

    private Contact getContact(String supplierID) {
        System.out.println("Enter contact name:");
        String name = "";
        String phoneNum = "";
        name = sc.nextLine();
        while (name.equals("") || name.charAt(0) == ' ' || !bl.legalName(name)) {
            System.out.println("name cannot be empty, start with a space or contain characters that aren't letters.");
            System.out.println("try again or enter 'back' to return to back");
            name = sc.nextLine();
            if (name.equals("main")) {
                return null;
            }
        }
        System.out.println("Please phone number");
        phoneNum = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(phoneNum)) {
                System.out.println("Phone number must be a positive number, please enter again or enter 'back' to go back");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return null;
                }
            }
            else if (bl.contactExistsWithSupp(supplierID, phoneNum)) {
                System.out.println("Contact already exists, please try again or enter 'back' to go back.");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return null;
                }
            } else {
                ret = true;
            }
        }
        return new Contact(name, phoneNum);
    }

    private Item getItem(String supplierID) {
        String catalogNum = "";
        boolean good = true;
        System.out.println("Please enter item catalog number:");
        while (good) {
            catalogNum = getNumber("Catalog Number");
            if (catalogNum.equals("back")) {
                return null;
            }
            else if (!supplierID.equals("")) {
                good = bl.ItemExists(supplierID, catalogNum);
                if (good) {
                    System.out.println("Item already exists, please try again");
                }
            }
            else {
                good = false;
            }
        }
        System.out.println("Please enter item name");
        String name = sc.nextLine();
        System.out.println("Please enter item price:");
        double price = 0;
        String sPrice = sc.nextLine();
        while (!bl.isPositiveDouble(sPrice)) {
            System.out.println("Price must be a positive number. Try again or enter back to go back");
            sPrice = sc.nextLine();
            if (sPrice.equals("back")) {
                return null;
            }
        }
        price = Double.parseDouble(sPrice);
        return new Item(catalogNum, price, name);
    }

    public static void main(String[] args) {
        PL_Manager manager = new PL_Manager();
        manager.main();
    }

    public void addDiscount() {
        String id = getSupplierId();
        System.out.println("Enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system. Try again or enter back to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        System.out.println("Enter quantity:");
        String sQuantity = "";
        int quantity = 0;
        sQuantity = getNumber("Quantity");
        while ((quantity = Integer.parseInt(sQuantity)) < 0) {
            System.out.println("Quantity must be a positive number. Try again or enter back to go back");
            sQuantity = sc.nextLine();
            if (sQuantity.equals("back")) {
                return;
            }
        }
        System.out.println("Enter discount(in percents):");
        double discount = 0;
        String sdiscount = sc.nextLine();
        while (!bl.isPositiveDouble(sdiscount)) {
            System.out.println("discount must be a positive number. Try again or enter back to go back");
            sdiscount = sc.nextLine();
            if (sdiscount.equals("back")) {
                return;
            }
        }
        discount = Double.parseDouble(sdiscount);
        if(bl.insertToDiscounts(id, new Discount(catalogNum, quantity, discount))){
            System.out.println("Discount was added successfully");
        } else {
            System.out.println("Error adding discount. Discount already exists");
        }
    }

    public void deleteDiscount() {
        String id = getSupplierId();
        System.out.println("Enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system. Try again or enter back to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        System.out.println("Enter quantity:");
        String sQuantity = "";
        int quantity = 0;
        sQuantity = getNumber("Quantity");
        while ((quantity = Integer.parseInt(sQuantity)) < 0) {
            System.out.println("quantity must be a positive number. Try again or enter back to go back");
            sQuantity = getNumber("Quantity");
            if (sQuantity.equals("back")) {
                return;
            }
        }
       if(bl.deleteFromDiscounts(id, catalogNum, quantity)){
    	     System.out.println("Discount was deleted successfully");
       } else {
           System.out.println("Error deleting discount");
       }
    }

}
